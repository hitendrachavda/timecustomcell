//
//  ViewController.swift
//  TimeCollectionCell
//
//  Created by Mac on 25/02/23.
//

import UIKit


class TimeCell : UICollectionViewCell {
    @IBOutlet weak var lblTime : UILabel?

}

struct TimeOjbect {
    var time : String
}

extension UIScrollView {
    func updateContentView() {
        contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
    }
}
class ViewController: UIViewController {

    @IBOutlet weak var collectionView : UICollectionView?
    @IBOutlet weak var collectionViewHeight : NSLayoutConstraint?

    @IBOutlet weak var collectionView2 : UICollectionView?
    @IBOutlet weak var collectionViewHeight2 : NSLayoutConstraint?
    
    var timeArray : [TimeOjbect] = [TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM"),TimeOjbect(time: "8:00 AM")]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView?.layoutIfNeeded()
        let contentSize = collectionView?.contentSize
        collectionViewHeight?.constant = contentSize!.height
        let contentSize2 = collectionView2?.contentSize
        collectionViewHeight2?.constant = contentSize2!.height

    }


}
extension ViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.timeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{

        let width = collectionView.frame.size.width - 40
        return (CGSize(width: width/4, height: 40))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCell", for: indexPath as IndexPath) as! TimeCell
        let timeob = self.timeArray[indexPath.row]
        cell.lblTime?.text = timeob.time
        return cell
    }
}
